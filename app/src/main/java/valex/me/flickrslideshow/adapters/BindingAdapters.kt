package valex.me.flickrslideshow.adapters

import android.databinding.BindingAdapter
import android.graphics.Bitmap
import android.graphics.Color
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.animation.GlideAnimation
import com.bumptech.glide.request.target.SimpleTarget
import valex.me.flickrslideshow.R
import valex.me.flickrslideshow.activities.PhotoFullscreenActivity
import valex.me.flickrslideshow.activities.SlideshowActivity
import valex.me.flickrslideshow.models.FlickrPhoto
import valex.me.flickrslideshow.utils.shouldCleanCache
import valex.me.flickrslideshow.viewmodels.SlideshowViewModel
import valex.me.flickrslideshow.views.TouchImageView

@BindingAdapter("url")
fun bindTouchImageUrl(imageView: TouchImageView, url: String) {
    val skipCache = shouldCleanCache(imageView.context)
    Glide
        .with(imageView.context)
        .load(url)
        .asBitmap()
        .diskCacheStrategy(DiskCacheStrategy.SOURCE)
        .into(object : SimpleTarget<Bitmap>() {
            override fun onResourceReady(resource: Bitmap?, glideAnimation: GlideAnimation<in Bitmap>?) {
                imageView.setImageBitmap(resource)
            }
        })
}

@BindingAdapter("url")
fun bindImageUrl(imageView: ImageView, url: String) {
    val skipCache = shouldCleanCache(imageView.context)
    Glide
        .with(imageView.context)
        .load(url)
        .asBitmap()
        .diskCacheStrategy(DiskCacheStrategy.SOURCE)
        .into(imageView)
}

@BindingAdapter("items")
fun bindPagerItems(viewPager: ViewPager, items: ArrayList<FlickrPhoto>) {
    if (viewPager.adapter == null) {
        val fragmentManager = (viewPager.context as AppCompatActivity).supportFragmentManager
        val adapter = SlideshowPagerAdapter(fragmentManager, items)
        viewPager.adapter = adapter
        viewPager.currentItem = (viewPager.context as SlideshowActivity).model?.currentIndex ?: 0
    }
    else {
        val adapter = viewPager.adapter as SlideshowPagerAdapter
        adapter.setItems(items)
    }
}

@BindingAdapter("callbacks")
fun bindModel(viewPager: ViewPager, model: SlideshowViewModel) {
    viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {

        override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}

        override fun onPageScrollStateChanged(state: Int)
            = model.onPagerScrollStateChanged(state)

        override fun onPageSelected(position: Int)
            = model.onPagerPageSelected(position)
    })
    viewPager.setPageTransformer(false, { page, position ->
        if(position >= -1 && position <= 1) {
            page.findViewById(R.id.image).translationX = -position * page.width/2
        }
    })
    model.pagerReference = viewPager
}

@BindingAdapter("onClick")
fun bindOnClick(view: View, photo: FlickrPhoto) {
    view.setOnClickListener {
        PhotoFullscreenActivity.start(view.context, photo)
    }
}

@BindingAdapter("progress")
fun bindProgress(progressBar: ProgressBar, progress: Int) {
    progressBar.progress = progress
}

@BindingAdapter("animatedVisibility")
fun bindAimatedVisibility(view: View, isVisible: Boolean) {
    val isPlayImage = view.tag == "play"
    val newAlpha = if (isVisible) 1F else 0F
    val newRotationPlay = if (isVisible) 0F else 180F
    val newRotationPause = if (isVisible) 180F else 0F

    view.animate().alpha(newAlpha).rotation(if (isPlayImage) newRotationPlay else newRotationPause).start()
}

@BindingAdapter("tintColor")
fun bindTintColor(imageView: ImageView, color: Int) {
    imageView.setColorFilter(color)
}