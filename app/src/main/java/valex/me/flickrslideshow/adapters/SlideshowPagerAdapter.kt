package valex.me.flickrslideshow.adapters

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import valex.me.flickrslideshow.fragments.PhotoFragment
import valex.me.flickrslideshow.models.FlickrPhoto

class SlideshowPagerAdapter(fragmentManager: FragmentManager,
                            private var _items: ArrayList<FlickrPhoto> = ArrayList()) : FragmentPagerAdapter(fragmentManager) {

    fun setItems(items: ArrayList<FlickrPhoto>) {
        _items = items
        notifyDataSetChanged()
    }

    override fun getItem(position: Int): Fragment = PhotoFragment.newInstance(_items[position])

    override fun getCount(): Int = _items.size
}
