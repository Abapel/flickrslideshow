package valex.me.flickrslideshow.models

import android.databinding.BaseObservable
import valex.me.flickrslideshow.utils.gson

class FlickrPhoto(var id: String = "",
                  var owner: String = "",
                  var secret: String = "",
                  var farm: String = "",
                  var title: String = "",
                  var server: String = ""): BaseObservable() {

    fun toJson() = gson.toJson(this)

    fun getUrl(): String = "https://farm$farm.staticflickr.com/$server/${id}_${secret}_b.jpg"

    companion object {

        fun fromJson(json: String): FlickrPhoto = gson.fromJson(json, FlickrPhoto::class.java)

        fun none(): FlickrPhoto = FlickrPhoto()
    }
}
