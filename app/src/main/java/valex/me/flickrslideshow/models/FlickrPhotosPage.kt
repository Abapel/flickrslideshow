package valex.me.flickrslideshow.models

class FlickrPhotosPage(var page: Int = 0,
                       var pages: Int = 0,
                       var perpage: Int = 0,
                       var total: Int = 0,
                       var photo: ArrayList<FlickrPhoto> = ArrayList())
