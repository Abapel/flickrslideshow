package valex.me.flickrslideshow.models

class FlickrUser(var username: FlickrContentContainer = FlickrContentContainer(),
                 var realname: FlickrContentContainer = FlickrContentContainer(),
                 var location: FlickrContentContainer = FlickrContentContainer(),
                 var profileurl: FlickrContentContainer = FlickrContentContainer(),
                 var mobileurl: FlickrContentContainer = FlickrContentContainer()) {

    fun getName(): String = if (realname._content.isNotEmpty()) realname._content else username._content

    fun getUrl(): String = mobileurl._content

    fun isExist(): Boolean = username._content.isNotEmpty()

    companion object {
        fun none(): FlickrUser = FlickrUser()
    }
}

class FlickrContentContainer(var _content: String = "")
