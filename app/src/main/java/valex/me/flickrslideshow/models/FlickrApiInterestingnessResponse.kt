package valex.me.flickrslideshow.models

class FlickrApiInterestingnessResponse(var photos: FlickrPhotosPage = FlickrPhotosPage(),
                                       var stat: String = "") {

    fun isSuccessful(): Boolean = stat == "ok"

    fun getAllPhotos(): ArrayList<FlickrPhoto> = photos.photo
}
