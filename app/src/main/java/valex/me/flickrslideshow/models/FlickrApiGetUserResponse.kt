package valex.me.flickrslideshow.models

class FlickrApiGetUserResponse(var stat: String = "",
                               var person: FlickrUser = FlickrUser()) {

    fun isSuccessful(): Boolean = stat == "ok"
}
