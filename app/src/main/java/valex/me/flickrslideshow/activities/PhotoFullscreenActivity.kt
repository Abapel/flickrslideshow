package valex.me.flickrslideshow.activities

import android.content.Context
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import valex.me.flickrslideshow.R
import valex.me.flickrslideshow.databinding.ActivityPhotoFullscreenBinding
import valex.me.flickrslideshow.models.FlickrPhoto

class PhotoFullscreenActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding = DataBindingUtil.setContentView<ActivityPhotoFullscreenBinding>(this, R.layout.activity_photo_fullscreen)
        binding.photo = getExtraPhoto()
    }

    fun getExtraPhoto(): FlickrPhoto? {
        if (intent.hasExtra(EXTRA_PHOTO)) {
            val json = intent.getStringExtra(EXTRA_PHOTO)
            return FlickrPhoto.fromJson(json)
        }
        return null
    }

    companion object {

        private val EXTRA_PHOTO = "EXTRA_PHOTO"

        fun start(context: Context, photo: FlickrPhoto) {
            val intent = Intent(context, PhotoFullscreenActivity::class.java)
            intent.putExtra(EXTRA_PHOTO, photo.toJson())
            context.startActivity(intent)
            (context as AppCompatActivity).overridePendingTransition(0, 0)
        }
    }
}