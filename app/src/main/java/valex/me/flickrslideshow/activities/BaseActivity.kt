package valex.me.flickrslideshow.activities

import android.support.v7.app.AppCompatActivity

open  class BaseActivity : AppCompatActivity()