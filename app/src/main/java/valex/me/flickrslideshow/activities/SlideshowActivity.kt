package valex.me.flickrslideshow.activities

import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import com.bumptech.glide.Glide
import valex.me.flickrslideshow.R
import valex.me.flickrslideshow.databinding.ActivitySlideshowBinding
import valex.me.flickrslideshow.services.CleanCacheService
import valex.me.flickrslideshow.utils.shouldCleanCache
import valex.me.flickrslideshow.viewmodels.SlideshowViewModel

class SlideshowActivity : BaseActivity() {

    var model: SlideshowViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding = DataBindingUtil.setContentView<ActivitySlideshowBinding>(this, R.layout.activity_slideshow)
        model = SlideshowViewModel(this)
        binding.model = model
    }

    override fun onResume() {
        super.onResume()

        model?.restoreState()
        model?.syncTimer()
    }

    override fun onPause() {
        super.onPause()

        model?.saveState()
        model?.stopTimer()
    }

    override fun onDestroy() {
        cleanCacheIfNeed()
        super.onDestroy()
    }

    fun cleanCacheIfNeed() {
        if (shouldCleanCache(this)) {
            CleanCacheService.cleanCache(this)
        }
    }
}