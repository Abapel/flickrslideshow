package valex.me.flickrslideshow.fragments

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import valex.me.flickrslideshow.R
import valex.me.flickrslideshow.databinding.FragmentPhotoBinding
import valex.me.flickrslideshow.models.FlickrPhoto

class PhotoFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val binding = DataBindingUtil.inflate<FragmentPhotoBinding>(inflater, R.layout.fragment_photo, container, false)
        binding.photo = getExtraPhoto()

        return binding.root
    }

    fun getExtraPhoto(): FlickrPhoto? {
        if (arguments.containsKey(KEY_PHOTO)) {
            val json = arguments.getString(KEY_PHOTO)
            return FlickrPhoto.fromJson(json)
        }
        return null
    }

    companion object {

        private val KEY_PHOTO = "KEY_PHOTO"

        fun newInstance(photo: FlickrPhoto): PhotoFragment {
            val fragment = PhotoFragment()
            val args = Bundle()
            args.putString(KEY_PHOTO, photo.toJson())
            fragment.arguments = args
            return fragment
        }
    }
}
