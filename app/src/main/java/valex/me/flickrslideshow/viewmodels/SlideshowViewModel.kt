package valex.me.flickrslideshow.viewmodels

import android.content.Context
import android.databinding.BaseObservable
import android.databinding.Bindable
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.view.View
import valex.me.flickrslideshow.BR
import valex.me.flickrslideshow.api.API
import valex.me.flickrslideshow.api.RetrofitCallback
import valex.me.flickrslideshow.models.FlickrApiGetUserResponse
import valex.me.flickrslideshow.models.FlickrApiInterestingnessResponse
import valex.me.flickrslideshow.models.FlickrPhoto
import valex.me.flickrslideshow.models.FlickrUser
import java.util.*
import kotlin.concurrent.fixedRateTimer
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import valex.me.flickrslideshow.utils.*

val SECONDS_PER_PHOTO = 5L
val PROGRESS_STEP = 1L
val MAX_PROGRESS = 100L

class SlideshowViewModel(var context: Context) : BaseObservable() {

    var timer: Timer? = null
    var currentPage: Int = 0
    var currentIndex: Int = 0
    var touchedByUser: Boolean = false
    var loading: Boolean = false

    @Bindable var currentPhoto: FlickrPhoto = FlickrPhoto.none()
    @Bindable var currentUser: FlickrUser = FlickrUser.none()
    @Bindable var allPhotos: ArrayList<FlickrPhoto> = ArrayList()
    @Bindable var timerProgress: Int = 0
    @Bindable var timerActive: Boolean = false
    @Bindable var introVisible: Boolean = false
    @Bindable var menuVisible: Boolean = false
    @Bindable var noInternet: Boolean = false
    @Bindable var shouldCleanCache: Boolean = true

    @Bindable val tintColor: Int = Color.parseColor("#D75813")

    var pagerReference: ViewPager? = null

    init {
        _setCurrentPhotoIndex(currentIndex)
        _setNoInternet(false)
    }

    //
    // TIMER STUFF
    //

    fun startTimer() {
        if (timer == null && timerActive) {
            timer = fixedRateTimer(period = 1000 * SECONDS_PER_PHOTO / (MAX_PROGRESS * PROGRESS_STEP), action = {
                timerProgress += PROGRESS_STEP.toInt()
                notifyPropertyChanged(BR.timerProgress)

                if (timerProgress > MAX_PROGRESS) {
                    resetProgress()
                    showNextSlide()
                }
            })
        }
    }

    fun stopTimer() {
        timer?.cancel()
        timer = null
    }

    fun resetProgress() {
        timerProgress = 0
        notifyPropertyChanged(BR.timerProgress)

        if (noInternet && _isLastPhoto()) {
            _setTimerActive(false)
            stopTimer()
        }

        log("reset progress")
    }

    fun syncTimer() {
        stopTimer()

        if (timerActive) {
            startTimer()
        }
    }

    fun _setTimerActive(active: Boolean) {
        timerActive = active
        notifyPropertyChanged(BR.timerActive)
    }

    //
    // USER ACTIONS
    //

    fun onMenuClicked(view: View) {
        menuVisible = !menuVisible
        notifyPropertyChanged(BR.menuVisible)
    }

    fun onUsernameClicked(view: View) {
        if (currentUser.isExist()) {
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(currentUser.getUrl()))
            view.context.startActivity(browserIntent)
        }
    }

    fun onGotItClicked(view: View) {
        showIntro(false)
    }

    fun onPlayPauseClicked(view: View) {

        _setTimerActive(!timerActive)

        if (timerActive) {
            startTimer()
        }
        else {
            stopTimer()
            resetProgress()
        }

        showIntro(false)
    }

    fun onTryAgainClicked(view: View) {
        _setNoInternet(false)
        loadNextPage()
    }

    fun onPagerScrollStateChanged(state: Int) {
        when (state) {
            ViewPager.SCROLL_STATE_IDLE -> {
                syncTimer()
            }
            ViewPager.SCROLL_STATE_DRAGGING -> {
                touchedByUser = true
                showIntro(false)
                stopTimer()
            }
        }
    }

    fun onPagerPageSelected(position: Int) {
        _setCurrentPhotoIndex(position)

        if (touchedByUser) {
            touchedByUser = false
            resetProgress()
        }

        if (_isLastPhoto()) {
            loadNextPage()
        }
    }

    fun onShouldCleanCacheClicked(view: View) {
        shouldCleanCache = !shouldCleanCache
        notifyPropertyChanged(BR.shouldCleanCache)

        setShouldCleanCache(context, shouldCleanCache)
    }

    //
    // STATE WORKFLOW
    //

    private val KEY_STATE = "KEY_STATE"

    private class StateHolder(var _currentPage: Int,
                              var _currentIndex: Int,
                              var _currentPhoto: FlickrPhoto,
                              var _currentUser: FlickrUser,
                              var _allPhotos: ArrayList<FlickrPhoto>,
                              var _timerProgress: Int,
                              var _timerActive: Boolean,
                              var _menuVisible: Boolean,
                              var _shouldCleanCache: Boolean) {

        fun toJson(): String = gson.toJson(this)

        companion object {

            fun fromJson(json: String): StateHolder? {
                if (json.isNotEmpty()) {
                    return gson.fromJson(json, StateHolder::class.java)
                }
                return null
            }
        }
    }

    fun saveState() {
        val stateHolder = StateHolder(currentPage,
                                      currentIndex,
                                      currentPhoto,
                                      currentUser,
                                      allPhotos,
                                      timerProgress,
                                      timerActive,
                                      menuVisible,
                                      shouldCleanCache)

        getPrefs(context).edit().putString(KEY_STATE, stateHolder.toJson()).apply()
    }

    fun restoreState() {
        val savedStateJson = getPrefs(context).getString(KEY_STATE, "")
        val savedState = StateHolder.fromJson(savedStateJson)
        if (savedState != null) {

            currentPage = savedState._currentPage
            currentIndex = savedState._currentIndex
            currentPhoto = savedState._currentPhoto
            currentUser = savedState._currentUser
            allPhotos = savedState._allPhotos
            timerProgress = savedState._timerProgress
            timerActive = savedState._timerActive
            menuVisible = savedState._menuVisible
            shouldCleanCache = savedState._shouldCleanCache

            notifyPropertyChanged(BR._all)
        }

        if (allPhotos.isEmpty()) {
            loadNextPage()
        }
    }

    //
    // MISC STUFF
    //

    fun showIntro(show: Boolean) {
        introVisible = show
        notifyPropertyChanged(BR.introVisible)
    }

    fun loadNextPage() {
        if (loading) {
            return
        }

        currentPage++

        log("Loading page $currentPage")

        loading = true

        API.getInstance().getInterestingPhotos(currentPage).enqueue(RetrofitCallback<FlickrApiInterestingnessResponse>({ response ->
            if (response != null && response.isSuccessful()) {

                if (allPhotos.isEmpty()) {
                    showIntro(true)
                }

                addPhotos(response.getAllPhotos())

                if (noInternet) {
                    _setNoInternet(false)
                    pagerReference?.adapter = null
                    notifyPropertyChanged(BR._all)
                }

                log("Page $currentPage loaded. Total photos = ${allPhotos.size}")

            }
            loading = false
        }, { error ->
            log(error)
            loading = false
            _setNoInternet(true)
            currentPage--
        }))
    }

    fun showNextSlide() {
        currentIndex++
        _setCurrentPhotoIndex(currentIndex)

        (context as AppCompatActivity).runOnUiThread {
            pagerReference?.currentItem = currentIndex
        }
    }

    fun addPhotos(photos: ArrayList<FlickrPhoto>) {

        allPhotos.removeAll(photos)
        allPhotos.addAll(photos)

        notifyPropertyChanged(BR.allPhotos)

        _setCurrentPhotoIndex(currentIndex)
    }

    fun _setCurrentPhotoIndex(index: Int) {
        if (allPhotos.size > index) {
            currentPhoto = allPhotos[index]
            notifyPropertyChanged(BR.currentPhoto)

            currentUser = FlickrUser.none()
            notifyPropertyChanged(BR.currentUser)

            currentIndex = index

            API.getInstance().getUserById(currentPhoto.owner).enqueue(RetrofitCallback<FlickrApiGetUserResponse>({ response ->
                if (response != null && response.isSuccessful()) {
                    currentUser = response.person
                    notifyPropertyChanged(BR.currentUser)
                }
            }, { error ->
                log(error)
            }))
        }
    }

    fun _setNoInternet(value: Boolean) {
        noInternet = value
        notifyPropertyChanged(BR.noInternet)
    }

    fun _setMenuVisible(value: Boolean) {
        menuVisible = value
        notifyPropertyChanged(BR.menuVisible)
    }

    fun _isLastPhoto(): Boolean = allPhotos.size - 1 == currentIndex
}