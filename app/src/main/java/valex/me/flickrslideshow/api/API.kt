package valex.me.flickrslideshow.api

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query
import valex.me.flickrslideshow.BuildConfig
import valex.me.flickrslideshow.models.FlickrApiGetUserResponse
import valex.me.flickrslideshow.models.FlickrApiInterestingnessResponse

interface API {

    @GET("?method=flickr.interestingness.getList")
    fun getInterestingPhotos(@Query("page") page: Int,
                             @Query("per_page") perPage: Int = 5,
                             @Query("api_key") apiKey: String = BuildConfig.FLICKR_API_KEY,
                             @Query("format") format: String = "json",
                             @Query("nojsoncallback") xz: Int = 1) : Call<FlickrApiInterestingnessResponse>

    @GET("?method=flickr.people.getInfo")
    fun getUserById(@Query("user_id") userId: String,
                    @Query("api_key") apiKey: String = BuildConfig.FLICKR_API_KEY,
                    @Query("format") format: String = "json",
                    @Query("nojsoncallback") xz: Int = 1) : Call<FlickrApiGetUserResponse>

    companion object {

        private val BASE_URL = "https://api.flickr.com/services/rest/"

        private var _instance: API? = null

        fun getInstance(): API {
            if (_instance == null) {

                val restAdapter = Retrofit.Builder()
                        .addConverterFactory(GsonConverterFactory.create())
                        .baseUrl(BASE_URL)
                        .build()

                _instance = restAdapter.create(API::class.java)
            }
            return _instance!!
        }
    }
}

class RetrofitCallback <T> (val onSuccess: (response: T?) -> Unit, val onError: (error: Throwable?) -> Unit = {}) : Callback<T> {
    override fun onResponse(call: Call<T>?, response: retrofit2.Response<T>?) {
        onSuccess.invoke(response?.body())
    }
    override fun onFailure(call: Call<T>?, t: Throwable?) {
        onError.invoke(t)
    }
}