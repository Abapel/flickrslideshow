package valex.me.flickrslideshow.utils

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import com.google.gson.Gson
import valex.me.flickrslideshow.viewmodels.SlideshowViewModel

fun log(any: Any?) = System.out.println("AppTag: ${any?.toString()}")

val gson = Gson()

fun getPrefs(context: Context): SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)

private val KEY_SHOULD_CLEAN_CACHE = "KEY_SHOULD_CLEAN_CACHE"

fun shouldCleanCache(context: Context): Boolean = getPrefs(context).getBoolean(KEY_SHOULD_CLEAN_CACHE, false)

fun setShouldCleanCache(context: Context, value: Boolean) = getPrefs(context).edit().putBoolean(KEY_SHOULD_CLEAN_CACHE, value).apply()