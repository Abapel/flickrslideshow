package valex.me.flickrslideshow.services

import android.app.IntentService
import android.content.Context
import android.content.Intent
import com.bumptech.glide.Glide

class CleanCacheService : IntentService("CleanCacheService") {

    override fun onHandleIntent(p0: Intent?) {
        Glide.get(this).clearDiskCache()
    }

    companion object {
        fun cleanCache(context: Context) {
            val intent = Intent(context, CleanCacheService::class.java)
            context.startService(intent)
        }
    }
}
